# Introduction

FrugalMapper is a cartridge for the SEGA Master System (SMS) console, designed to meet the following goals:

* Implement the minimal subset of the [SEGA mapper](https://www.smspower.org/Development/Mappers#TheSegaMapper) required to run most homebrew games.
* Support up to 512 KiB (4 Megabit) ROM size, that should fit most (if not all) homebrew productions.
* Use Flash memory instead of old EPROM chips.
* Use only discrete logic chips in addition to the Flash memory chip (and as few of them as possible), to avoid the problems finding components like CPLDs due to the semiconductor shortage.
* Be small and cheap to build.

![FrugalMapper PCB](sms_sl2map.webp)

# Capabilities

FrugalMapper implements the minimal subset of the SEGA Mapper required to run most homebrew games (and also many commercial games like Master of Darnkess, Castle of Illusion, etc.). The following is supported:

* The first 32 KiB of the ROM are mapped to slots 0 and 1 and cannot be changed.
* The slot 2 (from $8000 to $BFFF) can be bankswitched to any 16 KiB block in the ROM.
* To switch slot 2, the bank number must be written to any address from $E000 to $FFFF (and thus any program writing to the official SEGA Mapper slot 2 register at $FFFF will work).

# Things not supported and other warnings

* Bankswitching slots 0 and 1 is **not** supported. Trying to modify the bank in slots 0 or 1, will switch the bank in slot 2 instead!
* Writing to any address in the RAM mirror range ($E000 to $FFFF range) will also change the bank in slot 2 to whatever value is being written.
* On-cart RAM is **not** supported.
* Bank shifting is **not** supported (but who would need it anyway?).
* Currently the cart will **not** work on Mark III and Megadrive consoles (because these are missing one signal the cart requires).

# But your cart doesn't support battery backed RAM and I want to save!

Of course! I don't want to loose the progress after so many hours playing your game! Fortunately I got you covered, you can save data to the cartridge flash chip. Read carefully [Frugal Map Save](https://gitlab.com/doragasu/frugal-map-save) documentation, and then browse [this simple example](https://gitlab.com/doragasu/frugal-map-save-example) using the save code in a DevkitSMS project.

# Burning ROMs into the cart

To write ROMs into the cart, you can use the latest version of my [MegaWiFi Programmer hardware](https://gitlab.com/doragasu/mw-prog) and [MDMA client](https://gitlab.com/doragasu/mw-mdma-cli) that also support SMS carts. Any other programmer that can interface the SMS cart bus could also be used.

# PCB fabrication an assembly

If you want to get PCBs made and assembled, head to the [package registry](https://gitlab.com/doragasu/sms-sl2map/-/packages). There you can download an archive with the following:

* PCB Gerber files
* NC-Drill files
* Pick & Place pos files
* Bill of Materials (BOM)
* Schematics and PCB in PDF
* A BRD file for supported board viewers
* 2D and 3D renders of the board
* Netlists exported in several formats
* 3D models of the board in VRML and STEP file formats

For fabrication, you should need only the first four items in the list above, all others are for documentation. Any PCB fab house will usually ask for the PCB and NC-Drill files. If you want the components to get assembled, you will also need the BOM and the Pick & place files. The archives already include all you need to get the boards done packaged and ready to send for several well known manufacturers (such as JLCPCB and PCBWay). If you are using one of these manufacturers, you should just need to grab the zip file and send it to them to get quoted.

# License

FrugalMapper cartridge design is Open Hardware, licensed under the CERN-OHL-S license.
